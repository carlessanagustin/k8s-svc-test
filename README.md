# Kubernetes service test

Simple Kubernetes service ClusterIP & LoadBalancer test recipes and commands for Azure.

## Requirements

* A Kubernetes cluster running
* `helm`
* `kubectl`
* `make`
* `curl`

## Commands

* Review variables at the beginning of `Makefile` if needed.
* Install ingress: `make ingress_start`
* Test deployment: `make test_install`

> Install ingress options: `make (ingress_install | ingress_install_values | ingress_install_internal)`

## Troubleshooting

* Service: `make k8s_proxy` in terminal 1 then `make test_proxy` in terminal 2.
* Ingress: `make TEST_IP=<external ip> test_url`

> Ingress [values.yaml](https://github.com/kubernetes/ingress-nginx/blob/master/charts/ingress-nginx/values.yaml)
