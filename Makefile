INGRESS_NS ?= ingress-nginx
INGRESS_CLASS ?= nginx

HELM_RELEASE_NAME ?= current-ingress
HELM_CHART ?= ingress-nginx/ingress-nginx
HELM_VERSION ?= 3.29.0

TEST_PATH ?= nginx-test
TEST_NS ?= nginx-test
TEST_SVC ?= nginx-svc-cip

K8S_GET ?= pod,deployments,svc,ingress



test_install:
	kubectl apply -f ./nginx.yaml

test_uninstall:
	-kubectl delete -f ./nginx.yaml

test_watch:
	watch kubectl -n ${TEST_NS} get ${K8S_GET}

# TODO? L4 test
#INGRESS_NS ?= ingress
#TEST_IP_L4 = $(shell kubectl -n ${INGRESS_NS} get services ingress-release-ingress-nginx-controller -o jsonpath={'.status.loadBalancer.ingress[0].ip'})

# L7 test
TEST_IP = $(shell kubectl -n ${TEST_NS} get ingresses.extensions nginx-ingress -o jsonpath={'.status.loadBalancer.ingress[0].ip'})
test_url:
	@echo ">>> troubleshoot ingress"
	curl -Ik http://${TEST_IP}/${TEST_PATH}

# L4 test
TEST_IP = $(shell kubectl -n ${TEST_NS} get services nginx-svc-lb -o jsonpath={'.status.loadBalancer.ingress[0].ip'})
test_lb:
	@echo ">>> troubleshoot loadBalancer"
	curl -Ik http://${TEST_IP}


k8s_proxy:
	@echo ">>> troubleshoot service: step 1"
	kubectl proxy --port=8001

test_proxy:
	@echo ">>> troubleshoot service: step 2"
	curl http://127.0.0.1:8001/api/v1/namespaces/${TEST_NS}/services/${TEST_SVC}:http/proxy/${TEST_PATH}

ingress_start: repo_install ingress_install

repo_install:
	#helm repo add stable https://charts.helm.sh/stable
	helm repo add ingress-nginx https://kubernetes.github.io/ingress-nginx
	helm repo update

repo_uninstall:
	-helm repo remove stable
	-helm repo remove ingress-nginx

ingress_install:
	helm upgrade ${HELM_RELEASE_NAME} \
	${HELM_CHART} --install --cleanup-on-fail \
	--version ${HELM_VERSION} \
	--namespace ${INGRESS_NS} --create-namespace \
	--set controller.replicaCount=1 \
	--set controller.ingressClass=${INGRESS_CLASS}

ingress_install_values:
	helm upgrade ${HELM_RELEASE_NAME} \
	${HELM_CHART} --install --cleanup-on-fail \
	--version ${HELM_VERSION} \
	--namespace ${INGRESS_NS} --create-namespace \
	-f values.yaml

ingress_install_internal:
	helm upgrade ${HELM_RELEASE_NAME} \
	${HELM_CHART} --install --cleanup-on-fail \
	--version ${HELM_VERSION} \
	--namespace ${INGRESS_NS} --create-namespace \
	--set controller.replicaCount=3 \
	--set controller.ingressClass=${INGRESS_CLASS} \
    --set controller.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set defaultBackend.nodeSelector."beta\.kubernetes\.io/os"=linux \
    --set controller.admissionWebhooks.patch.nodeSelector."beta\.kubernetes\.io/os"=linux \
	--set controller.service.annotations."service\.beta\.kubernetes\.io/azure-load-balancer-internal"=true

ingress_uninstall:
	-helm uninstall --namespace ${INGRESS_NS} ${HELM_RELEASE_NAME}
	-kubectl delete ns ${INGRESS_NS}

ingress_watch:
	watch kubectl -n ${INGRESS_NS} get ${K8S_GET}

cleanup: test_uninstall ingress_uninstall repo_uninstall
